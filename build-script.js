const fs = require('fs-extra');
const concat = require('concat');
(async function build(){
  const files = [
    './dist/elementsApp/runtime-es2015.js',
    './dist/elementsApp/polyfills-es2015.js',
    /*'./dist/elementsApp/scripts.js',*/
    './dist/elementsApp/main-es2015.js',
  ]
  await fs.ensureDir('elements')
  await concat(files, 'elements/app-user-poll.js');
  await fs.copyFile('./dist/elementsApp/styles.css', 'elements/styles.css')
  // await fs.copyFile('./dist/elements/assets/', 'elements/assets/')
})()
